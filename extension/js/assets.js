const templateForm = document.createElement('template');
templateForm.innerHTML = `
    <figure>
        <figcaption>Jingle SNCF actuel:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/0564.mp3">
        </audio>
        <figcaption>Réveil mécanique, sonnerie courte:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1373.mp3">
        </audio>
        <figcaption>Cris d'enfants 4:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1228.mp3">
        </audio>
        <figcaption>Sifflements à la bouche:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1173.mp3">
        </audio>
        <figcaption>Bouc 2:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1381.mp3">
        </audio>
        <figcaption>Compte à rebours, talkie, en anglais:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1144.mp3">
        </audio>
        <figcaption>Alarme détecteur de fumée 3:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1153.mp3">
        </audio>
        <figcaption>Compte à rebours, talkie, en français:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1143.mp3">
        </audio>
        <figcaption>Homme siffle une mélodie:</figcaption>
        <audio
            controls
            src="https://lasonotheque.org/UPLOAD/mp3/1162.mp3">
        </audio>
    </figure>
    <a href="https://lasonotheque.org/">lasonotheque.org</a>

`;

export default class Assets extends HTMLElement {
    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});
        this._shadowRoot.appendChild(templateForm.content.cloneNode(true));
        

    }
    


} window.customElements.define('app-assets', Assets);
